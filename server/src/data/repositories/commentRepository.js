import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const commentLikeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: { id, deleted: false },
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: []
      }]
    });
  }
}

export default new CommentRepository(CommentModel);
