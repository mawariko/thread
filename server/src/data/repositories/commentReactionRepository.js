import {
  CommentReactionModel,
  CommentModel,
  UserModel
} from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: {
        userId,
        commentId
      },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getCommentReactions(commentId, isLike) {
    return this.model.findAll({
      where: {
        commentId,
        isLike
      },
      attributes: ['id'],
      include: {
        model: UserModel,
        attributes: ['id', 'username']
      },
      group: ['commentReaction.id', 'user.id']
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
