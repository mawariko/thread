import {
  PostReactionModel,
  PostModel,
  UserModel
} from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: {
        userId,
        postId
      },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getPostReactions(postId, isLike) {
    return this.model.findAll({
      where: {
        postId,
        isLike
      },
      attributes: ['id'],
      include: {
        model: UserModel,
        attributes: ['id', 'username']
      },
      group: ['postReaction.id', 'user.id']
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
