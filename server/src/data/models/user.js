export default (orm, DataTypes) => {
  const User = orm.define('user', {
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    status: {
      allowNull: true,
      type: DataTypes.STRING
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    temporaryCode: {
      allowNull: true,
      type: DataTypes.STRING
    },
    temporaryCodeExpiryDate: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {});

  return User;
};
