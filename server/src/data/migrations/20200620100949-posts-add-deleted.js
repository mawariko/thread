module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(
    transaction => queryInterface.addColumn('posts', 'deleted', {
      allowNull: false,
      type: Sequelize.BOOLEAN,
      defaultValue: false
    }, { transaction })
  ),

  down: queryInterface => queryInterface.sequelize.transaction(
    transaction => queryInterface.removeColumn('posts', 'deleted', { transaction })
  )
};
