module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('commentReactions', {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: Sequelize.literal('gen_random_uuid()')
    },
    isLike: {
      allowNull: false,
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
    userId: {
      type: Sequelize.UUID,
      references: {
        model: 'users',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    commentId: {
      type: Sequelize.UUID,
      references: {
        model: 'comments',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    }
  }),

  down: queryInterface => queryInterface.dropTable('commentReactions')
};
