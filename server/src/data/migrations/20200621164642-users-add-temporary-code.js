module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(
    transaction => Promise.all([
      queryInterface.addColumn('users', 'temporaryCode', {
        allowNull: true,
        type: Sequelize.STRING,
        defaultValue: null
      }, { transaction }),
      queryInterface.addColumn('users', 'temporaryCodeExpiryDate', {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: null
      }, { transaction })
    ])
  ),
  down: queryInterface => queryInterface.sequelize.transaction(
    transaction => Promise.all([
      queryInterface.removeColumn('users', 'temporaryCode', { transaction }),
      queryInterface.removeColumn('users', 'temporaryCodeExpiryDate', { transaction })
    ])
  )
};
