const nodemailer = require('nodemailer');

const email = 'mazie.herman@ethereal.email'; // Not using .env to make it easier for others to test'
const password = 'wqT7jPuguBYxJCVqqM';

export const sendEmail = async (to, subject, content) => {
  const transport = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: email,
      pass: password
    }
  });

  const message = {
    from: email,
    to,
    subject,
    html: content
  };

  return transport.sendMail(message);
};
