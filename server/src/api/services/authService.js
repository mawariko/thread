import { createToken } from '../../helpers/tokenHelper';
import { compare, encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';
import { sendEmail } from './emailService';

const crypto = require('crypto');

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

export const resetPassword = async ({ email }) => {
  const expiryPeriod = 30 * 60 * 1000;

  const user = await userRepository.getByEmail(email);
  if (!user) {
    const error = new Error('Incorrect email.');
    error.status = 401;
    throw error;
  }

  const code = crypto.randomBytes(16).toString('hex');
  const subject = 'Password reset';
  const link = `http://localhost:3000/update-password?email=${email}&code=${code}`;
  const message = `
    <h1>Password reset</h1>
    <p>You have requested a password reset.
    This is a temporary link which will stay valid for half an hour:</p>
    <p><a href=${link}>${link}</a></p>
  `;
  const expiryDate = new Date();
  expiryDate.setTime(expiryDate.getTime() + expiryPeriod);

  await userRepository.updateById(user.id, {
    temporaryCode: await encrypt(code),
    temporaryCodeExpiryDate: expiryDate
  });

  await sendEmail(email, subject, message);

  return true;
};

export const updatePassword = async ({ email, code, password }) => {
  const user = await userRepository.getByEmail(email);
  if (!user) {
    const error = new Error('Incorrect email.');
    error.status = 401;
    throw error;
  }

  if (!user.temporaryCode || (user.temporaryCodeExpiryDate < new Date())
    || !await compare(code, user.temporaryCode)) {
    const error = new Error('Link is invalid or expired.');
    error.status = 401;
    throw error;
  }

  if (!password) {
    const error = new Error('Password is required.');
    error.status = 401;
    throw error;
  }

  await userRepository.updateById(user.id, {
    password: await encrypt(password)
  });

  return true;
};
