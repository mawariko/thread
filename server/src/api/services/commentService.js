import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const update = async (commentId, newComment) => {
  await commentRepository.updateById(commentId, { body: newComment.body });
  return commentRepository.getCommentById(commentId);
};

export const deleteComment = async commentId => {
  await commentRepository.softDeleteById(commentId);

  return true;
};

export const getCommentById = id => commentRepository.getCommentById(id);

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : commentReactionRepository.getCommentReaction(userId, commentId);
};
// eslint-disable-next-line max-len
export const getReactions = async (commentId, isLike) => commentReactionRepository.getCommentReactions(commentId, isLike);
