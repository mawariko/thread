import userRepository from '../../data/repositories/userRepository';

const editableFields = ['username', 'email', 'imageId', 'status'];
const requiredFields = ['username', 'email'];

const validateEmail = email => {
  // eslint-disable-next-line
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const updateById = async (userId, data) => {
  Object.keys(data).forEach(prop => {
    if (!editableFields.includes(prop)) {
      throw new Error('Request contains unsupported fields');
    }
  });

  requiredFields.forEach(requiredField => {
    if (!data[requiredField]) {
      throw new Error(`${requiredField} is required`);
    }
  });

  if (!validateEmail) {
    throw new Error('Email is not valid');
  }

  await userRepository.updateById(userId, data);

  return userRepository.getUserById(userId);
};
