import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .get('/:id/likes', (req, res, next) => commentService.getReactions(req.params.id, true)
    .then(likes => res.send(likes))
    .catch(next))
  .get('/:id/dislikes', (req, res, next) => commentService.getReactions(req.params.id, false)
    .then(dislikes => res.send(dislikes))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => res.send(reaction))
    .catch(next))
  .put('/:id', (req, res, next) => commentService.update(req.params.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteComment(req.params.id)
    .then(result => res.send({ success: result }))
    .catch(next));

export default router;
