import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { resetPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ResetPasswordForm from 'src/components/ResetPasswordForm';

const ResetPasswordPage = ({ resetPassword: reset }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Reset your password
      </Header>
      <ResetPasswordForm resetPassword={reset} />
      <Message>
        Already with us?
        {' '}
        <NavLink exact to="/login">Sign In</NavLink>
        <br />
        New to us?
        {' '}
        <NavLink exact to="/registration">Sign Up</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

ResetPasswordPage.propTypes = {
  resetPassword: PropTypes.func.isRequired
};
const actions = { resetPassword };
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  null,
  mapDispatchToProps
)(ResetPasswordPage);
