import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { updatePassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header } from 'semantic-ui-react';
import UpdatePasswordForm from 'src/components/UpdatePasswordForm';
import queryString from 'query-string';

const UpdatePasswordPage = ({ updatePassword: update, location: { search } }) => {
  const { email, code } = queryString.parse(search);
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Update your password
        </Header>
        <UpdatePasswordForm email={email} code={code} updatePassword={update} />
      </Grid.Column>
    </Grid>
  );
};

UpdatePasswordPage.propTypes = {
  updatePassword: PropTypes.func.isRequired,
  location: PropTypes.objectOf(PropTypes.any).isRequired
};
const actions = { updatePassword };
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  null,
  mapDispatchToProps
)(UpdatePasswordPage);
