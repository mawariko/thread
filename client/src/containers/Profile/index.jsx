import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Button
} from 'semantic-ui-react';
import { uploadImage } from 'src/services/imageService';
import { updateCurrentUser } from '../../services/authService';
import { setUser } from './actions';

import styles from './styles.module.scss';

const Profile = ({ user, setUser: setGlobalUser }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [username, setUsername] = useState(user.username);
  const [email, setEmail] = useState(user.email);
  const [image, setImage] = useState(user.image ? user.image : undefined);
  const [profileStatus, setProfileStatus] = useState(user.status ? user.status : undefined);
  const [isUploading, setIsUploading] = useState(false);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isUsernameValid, setUsernameValid] = useState(true);

  const onSave = async () => {
    if (isEmailValid && isUsernameValid) {
      console.log(isEmailValid);
      setIsEditing(false);
      const updatedUser = await updateCurrentUser({
        username,
        email,
        imageId: image ? image.id : undefined,
        status: profileStatus });
      setGlobalUser(updatedUser);
    }
  };

  const onCancel = () => {
    setIsEditing(false);
    setUsername(user.username);
    setEmail(user.email);
    setImage(user.image ? user.image : undefined);
    setProfileStatus(user.status ? user.status : undefined);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const uploadedImage = await uploadImage(target.files[0]);
      setImage(uploadedImage);
    } finally {
      setIsUploading(false);
      // TODO: show error
    }
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={getUserImgLink(image)} size="medium" circular />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled={!isEditing}
          value={username}
          error={!isUsernameValid}
          onChange={e => setUsername(e.target.value)}
          onBlur={() => setUsernameValid(Boolean(username))}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled={!isEditing}
          value={email}
          error={!isEmailValid}
          onChange={e => setEmail(e.target.value)}
          onBlur={() => setEmailValid(validator.isEmail(email))}
        />
        <br />
        <br />
        <Input
          icon="pencil alternate"
          iconPosition="left"
          placeholder="Status"
          type="text"
          disabled={!isEditing}
          value={profileStatus}
          onChange={e => setProfileStatus(e.target.value)}
        />
        <br />
        {!isEditing && (
          <Button color="blue" onClick={() => setIsEditing(true)} className={styles.editButton}>
            Edit
          </Button>
        )}
        {isEditing && (
          <>
            <Button as="label" color="teal" loading={isUploading} disabled={isUploading} className={styles.editButton}>
              Change profile image
              <input name="image" type="file" onChange={handleUploadFile} hidden />
            </Button>
            <br />
            <Button.Group className={styles.editButton}>
              <Button color="blue" onClick={onSave}>Save</Button>
              <Button.Or />
              <Button onClick={onCancel}>Cancel</Button>
            </Button.Group>
          </>
        )}
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  setUser: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const mapDispatchToProps = dispatch => bindActionCreators({ setUser }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
