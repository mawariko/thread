import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Spinner from 'src/components/Spinner';
import { toggleEditPost, updatePost } from 'src/containers/Thread/actions';
import { Form, Modal, Button, Image, Icon } from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';

import styles from './styles.module.scss';

const EditPost = ({
  post,
  updatePost: update,
  toggleEditPost: toggle
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(
    post.image ? { imageLink: post.image.link } : undefined
  );
  const [isUploading, setIsUploading] = useState(false);

  const uploadImage = file => imageService.uploadImage(file);

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await update({ imageId: image?.imageId, body, id: post.id });
    toggle();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Form onSubmit={handleEditPost}>
              <Form.TextArea
                name="body"
                value={body}
                placeholder="What is the news?"
                onChange={ev => setBody(ev.target.value)}
              />
              {image?.imageLink && (
                <div className={styles.imageWrapper}>
                  <Image className={styles.image} src={image?.imageLink} alt="post" />
                </div>
              )}
              <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                <Icon name="image" />
                Attach image
                <input name="image" type="file" onChange={handleUploadFile} hidden />
              </Button>
              <Button floated="right" color="blue" type="submit">Save</Button>
            </Form>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

const actions = { toggleEditPost, updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(EditPost);
