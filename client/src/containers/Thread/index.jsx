/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditPost from 'src/containers/EditPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Loader, Dropdown } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts, loadMorePosts, likePost, dislikePost, toggleExpandedPost, toggleEditPost, addPost, deletePost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  excludeUser: undefined,
  getLiked: undefined,
  from: 0,
  count: 10
};

const filterOptions = [
  { value: 'all', text: 'Show all posts' },
  { value: 'mine', text: 'Show only my posts' },
  { value: 'not_mine', text: 'Show other\'s posts' },
  { value: 'liked', text: 'Show only posts I liked' }
];

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  editPost,
  hasMorePosts,
  addPost: createPost,
  deletePost: removePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  toggleEditPost: toggleEdit
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [activeFilter, setActiveFilter] = useState(filterOptions[0].value);
  const changeActiveFilter = (_, { value }) => {
    setActiveFilter(value);

    switch (value) {
      case 'mine':
        postsFilter.userId = userId;
        postsFilter.excludeUser = undefined;
        postsFilter.getLiked = undefined;
        break;
      case 'not_mine':
        postsFilter.userId = userId;
        postsFilter.excludeUser = 1;
        postsFilter.getLiked = undefined;
        break;
      case 'liked':
        postsFilter.userId = userId;
        postsFilter.excludeUser = undefined;
        postsFilter.getLiked = 1;
        break;
      default:
        // all
        postsFilter.userId = undefined;
        postsFilter.excludeUser = undefined;
        postsFilter.getLiked = undefined;
    }

    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Dropdown button value={activeFilter} options={filterOptions} onChange={changeActiveFilter} />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            toggleEditPost={toggleEdit}
            sharePost={sharePost}
            deletePost={removePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {editPost && <EditPost post={editPost} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  editPost: PropTypes.objectOf(PropTypes.any),
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  editPost: rootState.posts.editPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEditPost,
  addPost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
