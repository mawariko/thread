import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDIT_POSTT
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditPostAction = post => ({
  type: SET_EDIT_POSTT,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: {
      posts
    }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const toggleEditPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditPostAction(post));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const {
    id
  } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

// adding editPost
export const updatePost = post => async (dispatch, getRootState) => {
  const updatedPost = await postService.editPost(post, post.id);

  const {
    posts: {
      posts
    }
  } = getRootState();

  const updated = posts.map(p => ((p.id !== post.id) ? p : updatedPost));
  dispatch(setPostsAction(updated));
};

// delete post (soft)
export const deletePost = postId => async (dispatch, getRootState) => {
  const response = await postService.deletePost(postId);

  if (response.success) {
    const {
      posts: {
        posts,
        expandedPost
      }
    } = getRootState();

    const updated = posts.filter(p => p.id !== postId);
    dispatch(setPostsAction(updated));
    if (expandedPost && expandedPost.id === postId) {
      toggleExpandedPost();
    }
  }
};

const reactPost = (postId, isLike) => async (dispatch, getRootState) => {
  const {
    id,
    createdAt,
    updatedAt
  } = await postService.likePost(postId, isLike);

  const isDeleted = !id;
  const isToggled = id && createdAt !== updatedAt;

  let likeDiff;
  let dislikeDiff;
  if (isLike) {
    likeDiff = isDeleted ? -1 : 1;
    dislikeDiff = isToggled ? -1 : 0;
  } else {
    dislikeDiff = isDeleted ? -1 : 1;
    likeDiff = isToggled ? -1 : 0;
  }

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff // diff is taken from the current closure
  });

  const {
    posts: {
      posts,
      expandedPost
    }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const likePost = postId => reactPost(postId, true);

export const dislikePost = postId => reactPost(postId, false);

export const addComment = request => async (dispatch, getRootState) => {
  const {
    id
  } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: {
      posts,
      expandedPost
    }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

// update comment
export const updateComment = comment => async (dispatch, getRootState) => {
  const updatedComment = await commentService.editComment(comment, comment.id);

  const mapComments = post => ({
    ...post,
    comments: post.comments ? post.comments.map(c => (c.id !== comment.id ? c : updatedComment)) : []
  });

  const {
    posts: {
      posts,
      expandedPost
    }
  } = getRootState();

  const updated = posts.map(post => (post.id !== updatedComment.postId ? post : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

// delete Comment
export const deleteComment = (commentId, postId) => async (dispatch, getRootState) => {
  const response = await commentService.deleteComment(commentId);

  if (response.success) {
    const {
      posts: {
        posts,
        expandedPost
      }
    } = getRootState();

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: post.comments ? (post.comments.filter(c => c.id !== commentId)) : post.comments
    });

    const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));
    dispatch(setPostsAction(updated));
    if (expandedPost && expandedPost.id === postId) {
      dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
  }
};

const reactComment = (commentId, postId, isLike) => async (dispatch, getRootState) => {
  const {
    id,
    createdAt,
    updatedAt
  } = await commentService.likeComment(commentId, isLike);

  const isDeleted = !id;
  const isToggled = id && createdAt !== updatedAt;

  let likeDiff;
  let dislikeDiff;
  if (isLike) {
    likeDiff = isDeleted ? -1 : 1;
    dislikeDiff = isToggled ? -1 : 0;
  } else {
    dislikeDiff = isDeleted ? -1 : 1;
    likeDiff = isToggled ? -1 : 0;
  }

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + likeDiff,
    dislikeCount: Number(comment.dislikeCount) + dislikeDiff
  });

  const mapComments = post => (post.id !== postId ? post : ({
    ...post,
    comments: post.comments
      ? post.comments.map(c => (c.id !== commentId ? c : mapLikes(c)))
      : post.comments
  }));

  const {
    posts: {
      posts,
      expandedPost
    }
  } = getRootState();

  const updated = posts.map(mapComments);
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId && expandedPost.comments) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = (commentId, postId) => reactComment(commentId, postId, true);

export const dislikeComment = (commentId, postId) => reactComment(commentId, postId, false);
