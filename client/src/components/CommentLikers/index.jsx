import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { List, Dimmer, Loader } from 'semantic-ui-react';
import { getLikes, getDislikes } from '../../services/commentService';

const CommentLikers = ({ commentId, isLike }) => {
  const [reactions, setReactions] = useState(null);
  const callback = data => setReactions(data);

  if (!reactions) {
    if (isLike) {
      getLikes(commentId).then(callback);
    } else {
      getDislikes(commentId).then(callback);
    }
  }

  return reactions
    ? (
      <List>
        {reactions.map(reaction => (<List.Item key={reaction.id}>{reaction.user.username}</List.Item>))}
      </List>
    )
    : (<Dimmer active><Loader size="mini" /></Dimmer>);
};

CommentLikers.propTypes = {
  commentId: PropTypes.string.isRequired,
  isLike: PropTypes.bool.isRequired
};

export default CommentLikers;
