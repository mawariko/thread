import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { connect } from 'react-redux';
import PostLikers from '../PostLikers';

import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEditPost,
  sharePost,
  deletePost,
  currentUser
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  return (
    <>
      <Card style={{ width: '100%' }}>
        {image && <Image src={image.link} wrapped ui={false} />}
        <Card.Content>
          <Card.Meta>
            <span className="date">
              posted by
              {' '}
              {user.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          <Card.Description>
            {body}
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Popup
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => likePost(id)}
              >
                <Icon name="thumbs up" />
                {likeCount}
              </Label>
            )}
            on="hover"
            position="bottom left"
            disabled={parseInt(likeCount, 10) === 0}
          >
            <Popup.Content>
              <PostLikers postId={id} isLike />
            </Popup.Content>
          </Popup>
          <Popup
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => dislikePost(id)}
              >
                <Icon name="thumbs down" />
                {dislikeCount}
              </Label>
            )}
            on="hover"
            position="bottom left"
            disabled={parseInt(dislikeCount, 10) === 0}
          >
            <Popup.Content>
              <PostLikers postId={id} isLike={false} />
            </Popup.Content>
          </Popup>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
            <Icon name="comment" />
            {commentCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
            <Icon name="share alternate" />
          </Label>
          {currentUser.id === user.id && toggleEditPost
            ? (
              <>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleEditPost(id)}>
                  <Icon name="edit" />
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
                  <Icon name="remove" />
                </Label>
              </>
            )
            : ''}
        </Card.Content>
      </Card>
    </>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUser: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  currentUser: rootState.profile.user
});

export default connect(
  mapStateToProps
)(Post);

