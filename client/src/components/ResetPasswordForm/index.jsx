import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment } from 'semantic-ui-react';

const ResetPasswordForm = ({ resetPassword }) => {
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleReset = async () => {
    if (!isEmailValid) {
      return;
    }
    setIsLoading(true);
    try {
      await resetPassword(email);
      setIsLoading(false);
    } catch {
      // TODO: show error
      setIsLoading(false);
    }
  };

  return (
    <Form name="resetPasswordForm" size="large" onSubmit={handleReset}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Enter an email to get your new password"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Reset password
        </Button>
      </Segment>
    </Form>
  );
};

ResetPasswordForm.propTypes = {
  resetPassword: PropTypes.func.isRequired
};

export default ResetPasswordForm;
