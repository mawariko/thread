import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Button, Segment, Message } from 'semantic-ui-react';

const UpdatePasswordForm = ({ email, code, updatePassword }) => {
  const [password, setPassword] = useState('');
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const history = useHistory();

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleUpdate = async () => {
    if (!isPasswordValid) {
      return;
    }
    setIsLoading(true);
    try {
      await updatePassword({ email, code, password });
      setIsLoading(false);
      history.push('/login');
    } catch (e) {
      setErrorMessage(e.message);
      setIsLoading(false);
    }
  };

  return (
    <Form name="updatePasswordForm" size="large" onSubmit={handleUpdate}>
      <Segment>
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Your new password"
          type="password"
          onChange={ev => passwordChanged(ev.target.value)}
          error={!isPasswordValid}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Update password
        </Button>
        {errorMessage && (<Message>{errorMessage}</Message>)}
      </Segment>
    </Form>
  );
};

UpdatePasswordForm.propTypes = {
  updatePassword: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  code: PropTypes.string.isRequired
};

export default UpdatePasswordForm;
