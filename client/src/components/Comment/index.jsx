import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Card, Form, Button, Popup } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { updateComment, deleteComment } from 'src/containers/Thread/actions';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { connect } from 'react-redux';
import CommentLikers from '../CommentLikers';
import styles from './styles.module.scss';

const Comment = ({ comment: { id, body, createdAt, user, postId, likeCount, dislikeCount }, currentUser,
  updateComment: update, deleteComment: remove, likeComment, dislikeComment }) => {
  const [editting, setEditting] = useState(false);
  const [text, setText] = useState(body);

  const editComment = () => {
    setEditting(prevState => !prevState);
  };

  const updateCommentHandler = async () => {
    if (!text) {
      return;
    }
    await update({ id, body: text });
    setEditting(prevState => !prevState);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
      </CommentUI.Content>
      <Card.Content extra className="">
        <Popup
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id, postId)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
          on="hover"
          pinned
          position="bottom left"
          disabled={parseInt(likeCount, 10) === 0}
          className={styles.commentPopup}
        >
          <Popup.Content>
            <CommentLikers commentId={id} isLike />
          </Popup.Content>
        </Popup>
        <Popup
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => dislikeComment(id, postId)}
            >
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
          )}
          on="hover"
          pinned
          position="bottom left"
          disabled={parseInt(dislikeCount, 10) === 0}
          className={styles.commentPopup}
        >
          <Popup.Content>
            <CommentLikers commentId={id} isLike={false} />
          </Popup.Content>
        </Popup>
        <>
          {currentUser.id === user.id
            ? (
              <>
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.toolbarBtn}
                  onClick={() => editComment()}
                >
                  <Icon name="edit" />
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn}>
                  <Icon name="remove" onClick={() => remove(id, postId)} />
                </Label>
              </>
            )
            : ''}
        </>
        {editting
          ? (
            <Form onSubmit={updateComment}>
              <Form.TextArea
                value={text}
                placeholder="Type a comment..."
                onChange={ev => setText(ev.target.value)}
              />
              <Button
                type="submit"
                content="Update comment"
                labelPosition="right"
                icon="edit"
                onClick={() => updateCommentHandler()}
                primary
              />
            </Form>
          ) : ''}
      </Card.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUser: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  currentUser: rootState.profile.user
});

const actions = { updateComment, deleteComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Comment);
