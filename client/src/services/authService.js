import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const resetPassword = async email => {
  const response = await callWebApi({
    endpoint: '/api/auth/reset-password',
    type: 'POST',
    request: { email }
  });
  return response.json();
};

export const updatePassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/reset-password',
    type: 'PUT',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateCurrentUser = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/user',
    type: 'PUT',
    request
  });
  return response.json();
};
