import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

// edit comment
export const editComment = async (request, id) => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'PUT',
    request
  });
  return response.json();
};

// delete comment
export const deleteComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'DELETE'
  });
  return response.json();
};

export const likeComment = async (commentId, isLike = true) => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      commentId,
      isLike
    }
  });
  return response.json();
};

export const getLikes = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}/likes`,
    type: 'GET'
  });
  return response.json();
};

export const getDislikes = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}/dislikes`,
    type: 'GET'
  });
  return response.json();
};
