import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request
  });
  return response.json();
};

// edit post
export const editPost = async (request, id) => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'PUT',
    request
  });

  return response.json();
};

export const deletePost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'DELETE'
  });
  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const likePost = async (postId, isLike = true) => {
  const response = await callWebApi({
    endpoint: '/api/posts/react',
    type: 'PUT',
    request: {
      postId,
      isLike
    }
  });
  return response.json();
};

export const getLikes = async postId => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}/likes`,
    type: 'GET'
  });
  return response.json();
};

export const getDislikes = async postId => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}/dislikes`,
    type: 'GET'
  });
  return response.json();
};

// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);
